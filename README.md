Rock Paper Scissors Game
========================

Installation
------------

```bash
$ cd game
$ composer update
```

Execute
------------

Run game:

```bash
$ php bin/console app:rockPaperScissors -vv
```

Tests
-----

Run test

```bash
$ php bin/phpunit --testdox
```
