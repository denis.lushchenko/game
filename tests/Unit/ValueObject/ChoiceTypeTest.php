<?php

namespace App\Tests\Unit\ValueObject;

use App\ValueObject\ChoiceType;
use PHPUnit\Framework\TestCase;

class ChoiceTypeTest extends TestCase
{
    public function testConstructor(): void
    {
        $constants = (new \ReflectionClass(ChoiceType::class))->getConstants();

        foreach ($constants as $key => $value) {
            $this->assertSame($value, (new ChoiceType($value))->getValue());
        }
    }

    public function testDuplicateConstants(): void
    {
        $allConstants = array_values((new \ReflectionClass(ChoiceType::class))->getConstants());

        $notDuplicatedConstants = array_unique($allConstants);

        $this->assertCount(count($notDuplicatedConstants), $allConstants);
    }

    public function testConstantTypeUniqueness(): void
    {
        $this->assertCount(
            1,
            array_unique(
                array_map(
                    'gettype',
                    array_values(
                        (new \ReflectionClass(ChoiceType::class))->getConstants()
                    )
                )
            )
        );
    }

    public function testEquals(): void
    {
        $allConstants = array_values((new \ReflectionClass(ChoiceType::class))->getConstants());

        $constant1 = $allConstants[0];
        $constant2 = $allConstants[1];
        $constant3 = $allConstants[2];

        $vo1 = new ChoiceType($constant1);
        $vo2 = new ChoiceType($constant2);
        $vo3 = new ChoiceType($constant3);

        $this->assertFalse($vo1->equals($vo2));
        $this->assertFalse($vo2->equals($vo3));
        $this->assertFalse($vo3->equals($vo1));

        $this->assertTrue($vo1->equals($constant1));
        $this->assertFalse($vo1->equals($constant2));
        $this->assertFalse($vo1->equals($constant3));
        $this->assertTrue($vo2->equals($constant2));
        $this->assertFalse($vo2->equals($constant1));
        $this->assertFalse($vo2->equals($constant3));
        $this->assertTrue($vo3->equals($constant3));
        $this->assertFalse($vo3->equals($constant1));
        $this->assertFalse($vo3->equals($constant2));
    }
}
