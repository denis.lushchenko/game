<?php

namespace App\Tests\Unit\Command;

use App\Command\RockPaperScissorsCommand;
use App\Entity\Player;
use App\Service\RockPaperScissorsService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Helper\Table;

class RockPaperScissorsCommandTest extends TestCase
{
    public function testConfigure(): void
    {
        /** @var RockPaperScissorsService|MockObject $game */
        $game = $this->createMock(RockPaperScissorsService::class);

        $command = new RockPaperScissorsCommand($game);

        $this->assertSame('app:rockPaperScissors', $command->getName());
        $this->assertSame('Run RockPaperScissors', $command->getDescription());
        $this->assertCount(0, $command->getDefinition()->getArguments());
    }

    public function testInitGame(): void
    {
        $maxGameCount = 1;
        $player1Name  = 'Player A';
        $player2Name  = 'Player B';
        $gameResult   = ['Test'];

        /** @var RockPaperScissorsService|MockObject $game */
        $game = $this->createMock(RockPaperScissorsService::class);
        /** @var Table|MockObject $table */
        $table = $this->createMock(Table::class);

        $command = new RockPaperScissorsCommand($game);

        $game
            ->expects($this->once())
            ->method('play')
            ->with(
                $this->callback(
                    function (Player $player) use ($player1Name): bool {
                        $this->assertSame($player1Name, $player->getName());

                        return true;
                    }
                ),
                $this->callback(
                    function (Player $player) use ($player2Name): bool {
                        $this->assertSame($player2Name, $player->getName());

                        return true;
                    }
                )
            );

        $game
            ->expects($this->once())
            ->method('getGameResult')
            ->willReturn($gameResult);

        $table
            ->expects($this->once())
            ->method('setHeaders')
            ->with(['N', 'Player A Сhose', 'Player B Сhose', 'Winner'])
            ->willReturnSelf();

        $table
            ->expects($this->once())
            ->method('setRows')
            ->with($gameResult);

        $method = new \ReflectionMethod(RockPaperScissorsCommand::class, 'initGame');
        $method->setAccessible(true);

        $method->invoke($command, $table, $maxGameCount);
    }
}
