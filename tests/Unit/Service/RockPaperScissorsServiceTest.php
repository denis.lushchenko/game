<?php

namespace App\Tests\Unit\Service;

use App\Entity\Player;
use App\Service\RockPaperScissorsService;
use App\ValueObject\ChoiceType;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RockPaperScissorsServiceTest extends TestCase
{
    public function testPlayWithWinnerPlayer1(): void
    {
        $gameResult = true;

        $player1Name = 'Player A';

        $player1Choice = new ChoiceType(ChoiceType::SCISSORS);
        $player2Choice = new ChoiceType(ChoiceType::PAPER);

        $expectedResult = [
            [
                1,
                $player1Choice->getValue(),
                $player2Choice->getValue(),
                $player1Name
            ]
        ];

        /** @var Player|MockObject $player1 */
        $player1 = $this->createMock(Player::class);
        /** @var Player|MockObject $player2 */
        $player2 = $this->createMock(Player::class);

        /** @var RockPaperScissorsService|MockObject $service */
        $service = $this->getMockBuilder(RockPaperScissorsService::class)
            ->setMethods(['compare'])
            ->getMock();

        $player1
            ->expects($this->once())
            ->method('getChoice')
            ->willReturn($player1Choice);

        $player2
            ->expects($this->once())
            ->method('getChoice')
            ->willReturn($player2Choice);

        $service
            ->expects($this->once())
            ->method('compare')
            ->with($player1Choice, $player2Choice)
            ->willReturn($gameResult);

        $player1
            ->expects($this->once())
            ->method('getName')
            ->willReturn($player1Name);

        $service->play($player1, $player2);

        $property = new \ReflectionProperty(RockPaperScissorsService::class, 'gameResult');
        $property->setAccessible(true);

        $this->assertSame($expectedResult, $property->getValue($service));
    }

    public function testPlayWithWinnerPlayer2(): void
    {
        $gameResult = false;

        $player2Name = 'Player B';

        $player1Choice = new ChoiceType(ChoiceType::PAPER);
        $player2Choice = new ChoiceType(ChoiceType::ROCK);

        $expectedResult = [
            [
                1,
                $player1Choice->getValue(),
                $player2Choice->getValue(),
                $player2Name
            ]
        ];

        /** @var Player|MockObject $player1 */
        $player1 = $this->createMock(Player::class);
        /** @var Player|MockObject $player2 */
        $player2 = $this->createMock(Player::class);

        /** @var RockPaperScissorsService|MockObject $service */
        $service = $this->getMockBuilder(RockPaperScissorsService::class)
            ->setMethods(['compare'])
            ->getMock();

        $player1
            ->expects($this->once())
            ->method('getChoice')
            ->willReturn($player1Choice);

        $player2
            ->expects($this->once())
            ->method('getChoice')
            ->willReturn($player2Choice);

        $service
            ->expects($this->once())
            ->method('compare')
            ->with($player1Choice, $player2Choice)
            ->willReturn($gameResult);

        $player2
            ->expects($this->once())
            ->method('getName')
            ->willReturn($player2Name);

        $service->play($player1, $player2);

        $property = new \ReflectionProperty(RockPaperScissorsService::class, 'gameResult');
        $property->setAccessible(true);

        $this->assertSame($expectedResult, $property->getValue($service));
    }

    public function testPlayWithDraw(): void
    {
        $gameResult = null;

        $player1Choice = new ChoiceType(ChoiceType::PAPER);
        $player2Choice = new ChoiceType(ChoiceType::ROCK);

        $expectedResult = [
            [
                1,
                $player1Choice->getValue(),
                $player2Choice->getValue(),
                'Draw'
            ]
        ];

        /** @var Player|MockObject $player1 */
        $player1 = $this->createMock(Player::class);
        /** @var Player|MockObject $player2 */
        $player2 = $this->createMock(Player::class);

        /** @var RockPaperScissorsService|MockObject $service */
        $service = $this->getMockBuilder(RockPaperScissorsService::class)
            ->setMethods(['compare'])
            ->getMock();

        $player1
            ->expects($this->once())
            ->method('getChoice')
            ->willReturn($player1Choice);

        $player2
            ->expects($this->once())
            ->method('getChoice')
            ->willReturn($player2Choice);

        $service
            ->expects($this->once())
            ->method('compare')
            ->with($player1Choice, $player2Choice)
            ->willReturn($gameResult);

        $service->play($player1, $player2);

        $property = new \ReflectionProperty(RockPaperScissorsService::class, 'gameResult');
        $property->setAccessible(true);

        $this->assertSame($expectedResult, $property->getValue($service));
    }

    public function testCompareWithPlayer1ChoiceIsRockAndPlayer2ChoiceIsRock(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::ROCK);
        $player2ChoiceType = new ChoiceType(ChoiceType::ROCK);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertNull($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsRockAndPlayer2ChoiceIsScissors(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::ROCK);
        $player2ChoiceType = new ChoiceType(ChoiceType::SCISSORS);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertTrue($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsRockAndPlayer2ChoiceIsPaper(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::ROCK);
        $player2ChoiceType = new ChoiceType(ChoiceType::PAPER);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertFalse($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsScissorsAndPlayer2ChoiceIsRock(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::SCISSORS);
        $player2ChoiceType = new ChoiceType(ChoiceType::ROCK);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertFalse($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsScissorsAndPlayer2ChoiceIsScissors(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::SCISSORS);
        $player2ChoiceType = new ChoiceType(ChoiceType::SCISSORS);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertNull($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsScissorsAndPlayer2ChoiceIsPaper(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::SCISSORS);
        $player2ChoiceType = new ChoiceType(ChoiceType::PAPER);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertTrue($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsPaperAndPlayer2ChoiceIsRock(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::PAPER);
        $player2ChoiceType = new ChoiceType(ChoiceType::ROCK);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertTrue($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsPaperAndPlayer2ChoiceIsScissors(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::PAPER);
        $player2ChoiceType = new ChoiceType(ChoiceType::SCISSORS);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertFalse($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }

    public function testCompareWithPlayer1ChoiceIsPaperAndPlayer2ChoiceIsPaper(): void
    {
        $player1ChoiceType = new ChoiceType(ChoiceType::PAPER);
        $player2ChoiceType = new ChoiceType(ChoiceType::PAPER);

        $service = new RockPaperScissorsService();

        $method = new \ReflectionMethod(RockPaperScissorsService::class, 'compare');
        $method->setAccessible(true);

        $this->assertNull($method->invoke($service, $player1ChoiceType, $player2ChoiceType));
    }
}
