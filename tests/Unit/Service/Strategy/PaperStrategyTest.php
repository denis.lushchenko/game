<?php

namespace App\Tests\Unit\Service\Strategy;

use App\Service\Strategy\PaperStrategy;
use App\ValueObject\ChoiceType;
use PHPUnit\Framework\TestCase;

class PaperStrategyTest extends TestCase
{
    public function testPaperStrategy(): void
    {
        $paperType = new ChoiceType(ChoiceType::PAPER);
        $strategy  = new PaperStrategy();

        $this->assertSame($paperType->getValue(), $strategy->getChoice()->getValue());
    }
}
