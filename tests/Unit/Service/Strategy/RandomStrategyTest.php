<?php

namespace App\Tests\Unit\Service\Strategy;

use App\Service\Strategy\RandomStrategy;
use App\ValueObject\ChoiceType;
use PHPUnit\Framework\TestCase;

class RandomStrategyTest extends TestCase
{
    public function testRandomStrategy(): void
    {
        $strategy = new RandomStrategy();

        $this->assertInstanceOf(ChoiceType::class, $strategy->getChoice());
    }
}
