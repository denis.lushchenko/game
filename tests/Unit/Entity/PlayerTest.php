<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Player;
use App\Service\Strategy\PaperStrategy;
use App\ValueObject\ChoiceType;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    public function testPlayer(): void
    {
        $name      = 'Player';
        $paperType = new ChoiceType(ChoiceType::PAPER);

        $strategy = new PaperStrategy();

        $player = (new Player())
            ->setName($name)
            ->setStrategy($strategy);

        $this->assertSame($name, $player->getName());
        $this->assertSame($paperType->getValue(), $player->getStrategy()->getChoice()->getValue());
        $this->assertSame($paperType->getValue(), $player->getChoice()->getValue());
    }

    public function testPlayerWithoutStrategy(): void
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Property "strategy" is not defined');

        $name = 'Player';

        $player = (new Player())
            ->setName($name);

        $player->getChoice();
    }
}
