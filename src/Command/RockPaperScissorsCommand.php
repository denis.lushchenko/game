<?php

declare(strict_types = 1);

namespace App\Command;

use App\Entity\Player;
use App\Service\RockPaperScissorsService;
use App\Service\Strategy\PaperStrategy;
use App\Service\Strategy\RandomStrategy;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

class RockPaperScissorsCommand extends Command
{
    const MAX_GAME_COUNT = 100;

    protected static $defaultName = 'app:rockPaperScissors';

    /**
     * @var SymfonyStyle
     */
    private $io;

    /** @var RockPaperScissorsService */
    private $game;

    public function __construct(RockPaperScissorsService $game)
    {
        parent::__construct();

        $this->game = $game;
    }

    protected function configure(): void
    {
        $this->setDescription('Run RockPaperScissors');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    // php bin/console app:rockPaperScissors -vv
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('app-rockPaperScissors');

        $table = new Table($output);
        $this->initGame($table, self::MAX_GAME_COUNT);

        $event = $stopwatch->stop('app-rockPaperScissors');

        if ($output->isVerbose()) {
            $this->io->comment(
                sprintf(
                    'Elapsed time: %.2f ms / Consumed memory: %.2f MB',
                    $event->getDuration(),
                    $event->getMemory() / (1024 ** 2)
                )
            );
        }

        return 0;
    }

    protected function initGame(Table $table, int $maxGameCount): void
    {
        $playerA = (new Player())
            ->setName('Player A')
            ->setStrategy(new PaperStrategy());

        $playerB = (new Player())
            ->setName('Player B')
            ->setStrategy(new RandomStrategy());

        $i = 0;
        while ($i++ < $maxGameCount) {
            $this->game->play($playerA, $playerB);
        }

        $playerAName = sprintf('%s Сhose', $playerA->getName());
        $playerBName = sprintf('%s Сhose', $playerB->getName());

        $gameResult = $this->game->getGameResult();

        $table
            ->setHeaders(['N', $playerAName, $playerBName, 'Winner'])
            ->setRows($gameResult);

        $table->render();
    }
}
