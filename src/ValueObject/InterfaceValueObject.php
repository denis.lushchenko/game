<?php

declare(strict_types = 1);

namespace App\ValueObject;

interface InterfaceValueObject
{
    /**
     * @param object|int|string $value
     */
    public function equals($value): bool;

    /**
     * @return int|string
     */
    public function getValue();
}
