<?php

declare(strict_types = 1);

namespace App\ValueObject;

class ChoiceType extends AbstractValueObject
{
    public const ROCK     = 'Rock';
    public const PAPER    = 'Paper';
    public const SCISSORS = 'Scissors';
}
