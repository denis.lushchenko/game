<?php

declare(strict_types = 1);

namespace App\Service\Strategy;


use App\ValueObject\ChoiceType;

class PaperStrategy implements InterfaceStrategy
{
    public function getChoice(): ChoiceType
    {
        return new ChoiceType(ChoiceType::PAPER);
    }
}
