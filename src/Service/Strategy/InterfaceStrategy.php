<?php

declare(strict_types = 1);

namespace App\Service\Strategy;


use App\ValueObject\ChoiceType;

interface InterfaceStrategy
{
    public function getChoice(): ChoiceType;
}
