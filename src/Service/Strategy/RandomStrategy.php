<?php

declare(strict_types = 1);

namespace App\Service\Strategy;


use App\ValueObject\ChoiceType;

class RandomStrategy implements InterfaceStrategy
{
    public function getChoice(): ChoiceType
    {
        $choiceList = array_values(ChoiceType::getValueList());

        /** @noinspection PhpUnhandledExceptionInspection */
        return new ChoiceType($choiceList[random_int(0, 2)]);
    }
}
