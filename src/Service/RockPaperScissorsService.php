<?php

declare(strict_types = 1);

namespace App\Service;

use App\Entity\Player;
use App\ValueObject\ChoiceType;

class RockPaperScissorsService
{
    /** @var array */
    private $gameResult = [];

    public function play(Player $player1, Player $player2): void
    {
        $player1Choice = $player1->getChoice();
        $player2Choice = $player2->getChoice();

        $winner     = null;
        $gameResult = $this->compare($player1Choice, $player2Choice);

        if (is_null($gameResult)) {
            $winner = 'Draw';
        }

        if (is_null($winner)) {
            $winner = $gameResult ? $player1->getName() : $player2->getName();
        }

        $this->gameResult[] = [
            count($this->gameResult) + 1,
            $player1Choice->getValue(),
            $player2Choice->getValue(),
            $winner
        ];
    }

    public function getGameResult(): array
    {
        return $this->gameResult;
    }

    protected function compare(ChoiceType $player1Choice, ChoiceType $player2Choice): ?bool
    {
        switch (true) {
            case $player1Choice->equals(ChoiceType::ROCK) && $player2Choice->equals(ChoiceType::SCISSORS):
            case $player1Choice->equals(ChoiceType::PAPER) && $player2Choice->equals(ChoiceType::ROCK):
            case $player1Choice->equals(ChoiceType::SCISSORS) && $player2Choice->equals(ChoiceType::PAPER):
                return true;
            case $player1Choice->equals($player2Choice):
                return null;
            default:
                return false;
        }
    }
}
