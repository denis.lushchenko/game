<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Service\Strategy\InterfaceStrategy;
use App\ValueObject\ChoiceType;

class Player
{
    /** @var string */
    private $name;

    /** @var InterfaceStrategy|null */
    private $strategy;

    public function getStrategy(): ?InterfaceStrategy
    {
        return $this->strategy;
    }

    public function setStrategy(InterfaceStrategy $strategy): self
    {
        $this->strategy = $strategy;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getChoice(): ChoiceType
    {
        if (is_null($this->getStrategy())) {
            throw new \DomainException('Property "strategy" is not defined');
        }

        return $this->getStrategy()->getChoice();
    }
}
